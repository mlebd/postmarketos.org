# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later

imgs_url = "https://images.postmarketos.org/bpo"

# Both are without the service pack! Usually we don't generate new images after
# publishing a service pack, people are expected to install the service pack
# through update functionallity. This means, we might have just announced a
# service pack, but the images don't include it yet.
latest_release = "v22.06"
latest_release_title = "Release v22.06"

table = {
    "Main": {
        "pine64-pinephone": {
            "name": "PINE64 PinePhone",
        },
        "purism-librem5": {
            "name": "Purism Librem 5",
        },
    },
    "Community": {
        "arrow-db410c": {
            "name": "Arrow DragonBoard 410c",
        },
        "asus-me176c": {
            "name": "ASUS MeMO Pad 7",
        },
        "bq-paella": {
            "name": "BQ Aquaris X5",
        },
        "lenovo-a6000": {
            "name": "Lenovo A6000",
        },
        "lenovo-a6010": {
            "name": "Lenovo A6010",
        },
        "motorola-harpia": {
            "name": "Motorola Moto G4 Play",
        },
        "nokia-n900": {
            "name": "Nokia N900",
        },
        "odroid-hc2": {
            "name": "ODROID HC2",
        },
        "oneplus-enchilada": {
            "name": "OnePlus 6",
        },
        "oneplus-fajita": {
            "name": "OnePlus 6T",
        },
        "pine64-pinebookpro": {
            "name": "PINE64 PineBook Pro",
        },
        "pine64-pinephonepro": {
            "name": "PINE64 PinePhone Pro",
        },
        "pine64-pinetab": {
            "name": "PINE64 PineTab",
        },
        "pine64-rockpro64": {
            "name": "PINE64 RockPro 64",
        },
        "shift-axolotl": {
            "name": "SHIFT6mq",
        },
        "samsung-a3": {
            "name": "Samsung Galaxy A3 (2015)",
        },
        "samsung-a5": {
            "name": "Samsung Galaxy A5 (2015)",
        },
        "samsung-e7": {
            "name": "Samsung Galaxy E7",
        },
        "samsung-m0": {
            "name": "Samsung Galaxy S III (GT-I9300/SHW-M440S)",
        },
        "samsung-espresso3g": {
            "name": "Samsung Galaxy Tab 2 7.0",
        },
        "samsung-serranove": {
            "name": "Samsung Galaxy S4 Mini Value Edition",
        },
        "samsung-gt58": {
            "name": "Samsung Galaxy Tab 8.0 (2015)",
        },
        "samsung-gt510": {
            "name": "Samsung Galaxy Tab 9.7 (2015)",
        },
        "wileyfox-crackling": {
            "name": "Wileyfox Swift",
        },
        "xiaomi-scorpio": {
            "name": "Xiaomi Mi Note 2",
        },
        "xiaomi-beryllium": {
            "name": "Xiaomi Pocophone F1",
        },
        "xiaomi-wt88047": {
            "name": "Xiaomi Redmi 2",
        }
    }
}
