title: "pinephone issues with gtk4, keyboard"
date: 2022-05-09
---

Issues currently affecting the PinePhone on postmarketOS edge.

* gtk4 apps crash on launch (half the time?):
  [pma#1510](https://gitlab.com/postmarketOS/pmaports/-/issues/1510)
    * workaround in [pma!3122](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3122)

* Pinephone keyboard: problems with top row keys:
  [pma#1515](https://gitlab.com/postmarketOS/pmaports/-/issues/1515)
    * workaround in [pma!3114](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3114)
      but needs more work, help wanted if you're affected by this.

