title: "Unable to unlock disk after using on-device installer with pre-built images"
date: 2021-07-21
---
Pre-built images from 2021-07-16 and earlier for pmOS Edge are unable to
unlock the root filesystem if disk encryption is enabled during the on-device
installer.

This issue has been resolved in images built on 2021-07-20, which are the
latest images as of this post: https://images.postmarketos.org/bpo/edge/

Also see:
- [pmaports#1153](https://gitlab.com/postmarketOS/pmaports/-/issues/1153)
