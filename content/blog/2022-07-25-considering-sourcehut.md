title: "Considering SourceHut"
title-short: "Considering SourceHut"
date: 2022-07-25
preview: "2022-07/sourcehut.jpg"
---

[![](/static/img/2022-07/sourcehut_thumb.jpg){: class="wfull border" }](/static/img/2022-07/sourcehut.jpg)

After gitlab.com's
[recent changes](https://about.gitlab.com/blog/2022/03/24/efficient-free-tier/)
to the free tier, we have been seriously reconsidering if gitlab.com is still
the best development platform for postmarketOS. It was not at all an easy
question to answer for us, given how much we depend on a place for our code,
issues and CI. To reflect this rather long thought process we already had, we
decided to publish this short blog post with the most important thoughts we
currently arrived at together with a
[41 minute discussion on the podcast](https://cast.postmarketos.org/episode/20-Considering-SourceHut-special/)
where we go into a lot more detail.

> **UPDATE 2022-07-27:** we added a FAQ at the bottom, please read it before
> asking questions elsewhere. At the bottom you'll also find a link to the
> GitLab issue where we hope to discuss any feedback or concerns.

## Selfhosting is too much effort

It's important to us that the software we use could be selfhosted. But there's
a lot of maintenance effort that comes with running a selfhosted instance of
pretty much any code hosting or CI infrastructure related software: making sure
it is constantly available, has no unpatched vulnerabilities, doesn't run into
performance problems etc. All the time we would spend on that is time we can't
spend actually hacking on phones. We would very much like to avoid that.

So platforms where we would need to selfhost either the whole code forge or
CI for it are out of the picture. As you can guess from the title,
[SourceHut](https://sourcehut.org/) is the prime candidate right now and there
we could just use the official instance at [sr.ht](https://sr.ht/) for
everything, including CI. In fact, we have been using the CI component already
since end of 2019 to
[build all packages and images](/blog/2019/12/14/new-build-infra/).

## Why SourceHut

SourceHut is a great match for the
[postmarketOS principles](https://wiki.postmarketos.org/wiki/About_postmarketOS#Principles):

* AGPL licensed, no [Open-core model](https://en.wikipedia.org/wiki/Open-core_model)
* No tracking or advertising
* It's [fast](https://forgeperf.org/)

## Patch workflow

The most controversial point about this move would be the patch workflow. In
2022, many developers are now used to the merge request / pull request
submit and review workflow via web UI. On SourceHut, developers are supposed to
send patches via [git send-email](https://git-send-email.io/).

While there are [good reasons](https://drewdevault.com/2018/07/02/Email-driven-git.html)
for this approach, after many long discussions we realized that if we just
switched over one day and required everybody to use git send-email, we would
probably drive away many of the great contributors that made postmarketOS what
it is today.

But we can have the best of both worlds. The workflow of pushing your code to
your own repository on SourceHut and then creating the patch mail like a MR/PR
with a few clicks in the web UI is already implemented. Right now the review
process is only possible via mail, but compositing code reviews on the web is
planned as well
([lists.sr.ht#111](https://todo.sr.ht/~sircmpwn/lists.sr.ht/111)). We talked to
Drew DeVault (the main developer of SourceHut) and he told us that having the
whole review process in the web UI available is one of the top priorities for
SourceHut.

## Rollout plan

With all that in mind, we wrote the following tentative rollout plan. Note that
we only move pmbootstrap.git, not even the pmbootstrap issues, and no other
repository, starting from some time in August until December of 2022. Most
people are contributing to pmaports.git, so they won't be affected by this
change, but it will give us as maintainers a good chance to practice the email
based workflow with one of the most important postmarketOS git repositories.

Afterwards, in January of 2023 there's a chance to roll back if this experiment
failed. If it went well, we would move pmbootstrap issues and once the web UI
based patch review workflow is implemented, we would move pmaports.git and
everything else over as well.


### Timeline

#### 2022-08
* Migrate pmbootstrap.git repo (only the git repository, _not_ the issues
  etc)
* Adjust CI as necessary
* Get comfortable with the patch workflow
* By using it, find out what else would be missing for us

#### 2022-12
* v22.12 release

#### 2023-01
* Review the idea of moving to SourceHut based on experience with pmbootstrap.git
    * If it failed: abort here, move pmbootstrap.git back to gitlab.com
    * We didn't move the issues or anything else, so it's easy
* Check if web based patch workflow is implemented enough
    * If not, we work towards implementing it ourselves
    * Two months to implement before moving pmaports

#### 2023-02
* Import pmbootstrap issues
* Archive pmbootstrap repo
* pmbootstrap is now completely in sourcehut

#### 2023-03
* Move pmaports.git + issues at the same time
* Rewrite CI stuff, mrhlpr etc.
* Add tools for local CI checks for patch workflow
* Archive pmaports

#### 2023-05
* Move all other repositories + issues (fine if takes more than a month)

#### 2023-06
* v23.06 release

## What do you think?
If you have contributed to postmarketOS in the past, we are really curious
about what you think about all of the above. Do you like the idea? Did we miss
something? What are your hopes and concerns? We would be happy to read your
feedback in
[postmarketos#49](https://gitlab.com/postmarketOS/postmarketos/-/issues/49).

## FAQ

### I don't like the email workflow

As mentioned above at the end of the [patch workflow](#patch-workflow) section
and in more detail in
[Clayton's take](https://cast.postmarketos.org/episode/20-Considering-SourceHut-special/#1993.0)
in the podcast:

* SourceHut is prioritising to implement an entirely web-based flow for
  contributors.
* This is a hard-requirement for us as maintainers to ensure that postmarketOS
  is accessible to as many contributors as possible.
* We will not switch over pmaports.git (which is what most people actually
  interact with, the repository that has the package build recipes) before this
  is implemented.

### Why not Gitea/Codeberg/other GitHub-inspired forge?

We explored using Gitea or Codeberg, and eventually concluded that compared to
SourceHut the only benefit it would offer is the GitHub/Lab style contributor
workflow. This didn't seem like a compelling enough reason to pursue it.

Gitea doesn't have its own CI and thus would require us to use an external
service — like SourceHut CI. Codeberg's integrated CI is in a closed testing
phase and may break at any time
(see [here](https://codeberg.org/Codeberg-CI/request-access#disclaimers-and-caveats))
whereas we have been using SourceHut CI since end of 2019 for building packages
and images already and found it to be highly reliable.

Considering SourceHut's pledge to make contributing possible without email, and
the other bonuses of working with them directly, Gitea becomes a hard sell in
comparison.

We are definitely in a unique position given we already use SourceHut's
infrastructure — this isn't meant to be a competition between forges.

### What about a GitLab instance hosted elsewhere?

GitLab's Open-core licensing model doesn't align as well with the postmarketOS
principles as a fully libre service. It was a big improvement over the entirely
proprietary GitHub which we used before, but now that we are already
considering to switch platforms again, we might as well try to use something
that is again much closer aligned to our own values.

As mentioned, SourceHut carries a lot of other benefits such as the
potential to drastically improve productivity for both maintainers and
contributors, which we would not get by using another GitLab instance.
